# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* check_stock
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 9.0e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-01-28 04:10+0000\n"
"PO-Revision-Date: 2016-01-28 04:10+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: check_stock
#: code:addons/check_stock/models/sale.py:23
#, python-format
msgid "%s stock was not enough"
msgstr "%s 库存不够"

#: code:addons/check_stock/models/sale.py:24
#, python-format
msgid "Stock warning"
msgstr "库存警告"

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_product_product_application
#: model:ir.model.fields,field_description:check_stock.field_product_template_application
#: model:ir.ui.view,arch_db:check_stock.product_template_form_view
msgid "Application people"
msgstr "适用人群"

#. module: check_stock
#: model:ir.actions.act_window,name:check_stock.brand_action
#: model:ir.model,name:check_stock.model_check_stock_brand
#: model:ir.model.fields,field_description:check_stock.field_product_product_brand
#: model:ir.model.fields,field_description:check_stock.field_product_template_brand
#: model:ir.ui.menu,name:check_stock.menu_product_brand
#: model:ir.ui.view,arch_db:check_stock.product_template_form_view
msgid "Brand"
msgstr "品牌"

#. module: check_stock
#: model:ir.actions.act_window,name:check_stock.brand_category_action
#: model:ir.model,name:check_stock.model_check_stock_brand_category
#: model:ir.model.fields,field_description:check_stock.field_product_product_brand_category
#: model:ir.ui.menu,name:check_stock.menu_product_brand_category
#: model:ir.ui.view,arch_db:check_stock.view_brand_category_form
msgid "Brand Category"
msgstr "品类"

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_product_template_brand_category
#: model:ir.ui.view,arch_db:check_stock.product_template_form_view
msgid "Brand category"
msgstr "品类"

#. module: check_stock
#: model:ir.ui.view,arch_db:check_stock.view_order_form
msgid "Check stock"
msgstr "检查库存"

#. module: check_stock
#: selection:product.product,application:0
msgid "Children Clothes"
msgstr "童装"

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_create_uid
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_create_uid
msgid "Created by"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_create_date
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_create_date
msgid "Created on"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_display_name
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_display_name
msgid "Display Name"
msgstr ""

#. module: check_stock
#: model:ir.actions.act_window,name:check_stock.action_factory
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_factory
#: model:ir.model.fields,field_description:check_stock.field_res_partner_factory
#: model:ir.ui.menu,name:check_stock.menu_factory
#: model:ir.ui.view,arch_db:check_stock.facory_partner_form
#: model:ir.ui.view,arch_db:check_stock.view_brand_category_form
msgid "Factory"
msgstr "工厂"

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_id
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_id
msgid "ID"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand___last_update
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category___last_update
msgid "Last Modified on"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_write_uid
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_write_uid
msgid "Last Updated by"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_write_date
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_write_date
msgid "Last Updated on"
msgstr ""

#. module: check_stock
#: selection:product.product,application:0
msgid "Men clothes"
msgstr "成人男装"

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_category_name
#: model:ir.model.fields,field_description:check_stock.field_check_stock_brand_name
#: model:ir.ui.view,arch_db:check_stock.view_brand_category_form
msgid "Name"
msgstr "名字"

#. module: check_stock
#: model:ir.model,name:check_stock.model_res_partner
msgid "Partner"
msgstr ""

#. module: check_stock
#: model:ir.model,name:check_stock.model_product_product
msgid "Product"
msgstr "产品"

#. module: check_stock
#: model:ir.model,name:check_stock.model_product_template
msgid "Product Template"
msgstr ""

#. module: check_stock
#: model:ir.model,name:check_stock.model_sale_order
msgid "Sales Order"
msgstr ""

#. module: check_stock
#: model:ir.model.fields,field_description:check_stock.field_sale_order_stock
msgid "Stock"
msgstr "库存"

#. module: check_stock
#: selection:product.product,application:0
msgid "Women clothes"
msgstr "成人女装"
