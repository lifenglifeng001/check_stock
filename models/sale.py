# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp import exceptions
from openerp import _

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    stock = fields.Boolean('Stock')

    @api.one
    def action_check_stock(self):
        """ check product stock"""
        not_enough = {}
        for line in self.order_line:
            product_id = line.product_id
            if line.product_uom_qty > product_id.qty_available:
                not_enough[product_id.id] = product_id.name

        if not_enough:
            product_str = ', '.join(not_enough.values())
            warn = _("%s stock was not enough") % product_str
            raise exceptions.Warning((_('Stock warning'), warn))
        else:
            self.stock = True
