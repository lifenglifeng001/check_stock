# -*- coding: utf-8 -*-
{
    'name': "check_stock",

    'summary': """
        检查销售单库存
        添加品牌， 品类
    """,

    'description': """
        check stock
    """,

    'author': "LiFeng",
    'website': "",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'stock', 'product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/factory_view.xml',
        'views/brand_view.xml',
        'views/sale_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

    'application': True,
}
